package com.example.puza.retrofitbloggerapi;

public class Fetch {

    private String id;
    private String displayName;
    private String url;
    private String image;

    public Fetch(String id, String displayName, String url, String image) {
        this.id = id;
        this.displayName = displayName;
        this.url = url;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getUrl() {
        return url;
    }

    public String getImage() {
        return image;
    }
}
