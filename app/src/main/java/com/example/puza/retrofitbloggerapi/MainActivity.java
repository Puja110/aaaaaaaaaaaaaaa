package com.example.puza.retrofitbloggerapi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //setUpToolbar();




        final ListView listView = findViewById(R.id.listView);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Api api = retrofit.create(Api.class);

        Call<List<Fetch>> call = api.getFetch();

        call.enqueue(new Callback<List<Fetch>>() {
            @Override
            public void onResponse(Call<List<Fetch>> call, Response<List<Fetch>> response) {
                List<Fetch> fetches = response.body();

                String[] fetchUser = new String[fetches.size()];

                for (int i=0; i < fetches.size(); i++) {

                    fetchUser[i] = fetches.get(i).getId();
                    fetchUser[i] = fetches.get(i).getDisplayName();
                    fetchUser[i] = fetches.get(i).getUrl();
                    fetchUser[i] = fetches.get(i).getImage();
                }

                listView.setAdapter(
                        new ArrayAdapter<String>(
                                getApplicationContext(),
                                android.R.layout.simple_list_item_1,
                                fetchUser
                        )
                );
            }

            @Override
            public void onFailure(Call<List<Fetch>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
